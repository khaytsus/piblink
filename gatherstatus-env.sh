#!/bin/bash

## Networking vars
if="eth0"
ip="192.168.0.20"

## Disk full paths
path1="/"
minfree1="250"
path2="/tmp"
minfree2="3"
path3="/var/tmp"
minfree3="3"

## Overheating
maxtemp=60

## Fan control, unset these if you don't use this
templow=25
temphigh=40

# Don't touch this
envsourced=1
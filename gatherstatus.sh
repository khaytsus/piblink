#!/bin/bash

# Script that outputs a number based on some tests, which is then read by a
# secondary script that continuously runs and blinks the GPIO pin to indicate
# the value.  Keep in mind that unless you want to count a ton of blinks, keep
# the tests low.

# This is the simple script, so it only outputs a single value.  If you want to
# decode mulitple types of failures, use the gatherstatus.sh script instead

# 0 - Not initialized/scripts not running at all
# 1 - System booted
# 2 - Networking not up
# 3 - Networking up, but no connectivity
# 4 - Disk full
# 5 - Overheating
#   - Solid light; indicates this script is not running

# How long to sleep between gathering data
sleeptime=15s

# Status file that blinkgpio.py picks up
statusfile="/tmp/led-piblink.txt"

# Status file that blinkgpio.py picks up
fanfile="/tmp/fan-piblink.txt"

# Env file; make sure it's in the same directory as this script
parent=`dirname "$0"`
envfile="${parent}/gatherstatus-env.sh"

source ${envfile}

if [ "${envsourced}" != "1" ]; then
    echo "Did not load ${envfile} as expected, check that it exists"
    exit
fi

while [ 1 ]; do
    # System is booted if we're here
    curval=1

    # Refresh our env, so we can edit this on the fly
    source ${envfile}

    # Network connectivity test
    ifup=`ip a show ${if} up`

    if [ "${ifup}" != "" ]; then
        echo "Interface ${if} is up"
        ping -c 3 ${ip} &> /dev/null
        rc=$?
        if [ ${rc} -ne 0 ]; then
            echo "Can not ping ${ip}, rc ${rc}"
            # If network is up but no connectivity, 3
            curval=3
        fi
    else
        # If the network device isn't up at all, 2
        curval=2
    fi

    # Disk check, path 1
    diskfull=0
    if [ "${path1}" != "" ]; then
        free=`df -m ${path1} | grep "${path1}" | sed -e's/  */ /g' | cut -f 4 -d " "`
        echo "${path1} has ${free} free, minimum allowed ${minfree1}"
        if [ ${free} -lt ${minfree1} ]; then
            ((diskfull++))
        fi
    fi

    # Disk check, path 2
    if [ "${path2}" != "" ]; then
        free=`df -m ${path2} | grep "${path2}" | sed -e's/  */ /g' | cut -f 4 -d " "`
        echo "${path2} has ${free} free, minimum allowed ${minfree2}"
        if [ ${free} -lt ${minfree2} ]; then
            ((diskfull++))
        fi
    fi

    # Disk check, path 3
    if [ "${path3}" != "" ]; then
        free=`df -m ${path3} | grep "${path3}" | sed -e's/  */ /g' | cut -f 4 -d " "`
        echo "${path3} has ${free} free, minimum allowed ${minfree3}"
        if [ ${free} -lt ${minfree3} ]; then
            ((diskfull++))
        fi
    fi

    if [ ${diskfull} -gt 0 ]; then
        curval=4
    fi

    # Test for overheating
    temp=`vcgencmd measure_temp | cut -f 2 -d "=" | cut -f 1 -d "'"`
    temp_int=$(printf '%.0f\n' ${temp})
    echo "Temperature is ${temp} of maximum ${maxtemp} allowed"
    if [ ${temp_int} -gt ${maxtemp} ]; then
        echo "${temp} exceeds maximum of ${maxtemp}"
        curval=5
    fi

    # If the file exists, the fan will be off, otherwise it defaults to on
    # When the fan drops below the low temp, turn off the fan
    # But don't turn it back on until it reaches the high temp
    if [ "${templow}" != "" ] && [ "${temphigh}" != "" ]; then
        echo "Fan control:  Low: [${templow}] High: [${temphigh}]"
        if [ ${temp_int} -le ${templow} ]; then
            if [ ! -f ${fanfile} ]; then
                echo "Creating file, ${temp} is less than ${templow} which turns fan off"
                touch ${fanfile}
            fi
        fi

        if [ ${temp_int} -ge ${temphigh} ]; then
            if [ -f ${fanfile} ]; then
                echo "Removing file, ${temp} is greater than ${templow} which turns fan on"
                rm -f ${fanfile}
            fi
        fi
    fi

    echo "final value: [${curval}]"
    echo ""
    echo ${curval} > ${statusfile}
    sleep ${sleeptime}
done

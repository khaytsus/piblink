#!/usr/bin/env python3

import os.path
from os import path
import time

from gpiozero import LED
from time import sleep

# GPIO 21, conveniently at the bottom of the GPIO pins
redLED = LED(21)

# GPIO 14, conveniently inline with 5V and GND
fanGPIO = LED(14)

filepath = '/tmp/led-piblink.txt'
fanpath = '/tmp/fan-piblink.txt'

# Age in seconds before the file above is considered too old
maxage=30

while True:
  blinks=0
  timeon=0.5
  sleeptime=2.0

  # If the file does not exist, just turn on the led forever
  if not path.exists(filepath):
    print( 'on forever because file does not exist' )
    redLED.on()
  # Otherwise, see if our file is old, if so, also turn on led forever
  else:
    st=os.stat( filepath )    
    age=(time.time()-st.st_mtime)
    if age > maxage:
      print ( 'file age is ' + str(age) )
      print( 'on forever because old file' )
      redLED.on()
    # If our file looks up to date, read from it and blink
    else:
      status_file = open(filepath,'r')
      line=status_file.read().rstrip()
      # After stripping any newlines, try to convert to int with exception handling
      try:
        blinks=int(line)
        print('Blinks: ' + str(blinks))
        for i in range(blinks):
          print('led on ' + str(timeon))
          redLED.on()
          sleep(timeon)
          print('led off')
          redLED.off()
          sleep(timeon)
      except ValueError:
        print ( 'invalid line value, turning on LED' )
        redLED.on()

  # If the file exists turn oon the GPIO which turns off the fan since it defaults
  # to always running
  if path.exists(fanpath):
    print( 'Fan: Off' )
    fanGPIO.on()
  else:
    print( 'Fan: On' )
    fanGPIO.off()

  print( 'sleep ' + str(sleeptime) + '\n' )
  sleep(sleeptime)

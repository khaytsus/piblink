# piblink

## Overview
Blink a GPIO pin, presumably an LED, to indicate the status of the Pi.  If things are not running at all the LED won't be lit, if the status file isn't being updated the LED will be lit all the time.  Otherwise, it indicates the status of the Pi.  Normal is 1 blink which indicates it's online and able to reach the specified IP address and has no other faults.  There are other blinks for network connectivity, disk space and overheating as just proof of concept tests, although those really should be done with a tool like monit as necessary.  There is also optional rudamentary fan control, see the Fan Control section for more details if you need that.

I wrote this script because sometimes my headless Pi does not show up on the network and I have no idea why.  This should hopefully give me some sort of indication of what is going on.  For example if the network is up, but it can't reach the IP address it will blink three times letting me know perhaps the wifi is not connecting as expected.

To understand the blink results, see gatherstatus.sh for the list of values.  By default they are as follows
  * LED not lit - Not initialized/scripts not running at all
  * Solid LED - indicates this script is not running
  * 1 - System booted and everything is normal
  * 2 - Networking not up
  * 3 - Networking up, but no connectivity
  * 4 - Disk full
  * 5 - Overheating

## GPIO Defaults
blinkgpio.py defaults to 21 for the LED and 13 for the Fan control but you can change these in blinkgpio.py if you prefer.

## Files 

### gatherstatus.sh 
Collects status from the Pi and tests the things defined within it and outputs a number to a file defined in the script for the Python script to pick up and blink that many number of times.  This script is independent of the blinking script so that the blinking happens consistently regardless of any timeouts or such that may happen when doing any status tests.

### gatherstatus-env.sh 
Variables that will vary from setup to setup to make it easier to update the main scripts without re-editing for your personal config.  You will likely need to edit this for your setup.  Network interface, IP, temps, etc.  

### blinkgpio.py 
Python script that toggles a GPIO pin based on the file that gatherstatus.sh outputs.  This file also defines the GPIO pins being used so if you need to change those you would do it here.

## Script setup
Edit gatherstatus-env.sh and update for your network device, a lan device on your network to test that the Pi has connectivity, optionally some paths to test and their minimum free disk space in megabytes, the maximum temperature of the pi, etc.

You can change these to anything you like really, but these are the ones I decided I wanted.

The blinkgpio.py script and gatherstatus.sh need to run on startup and forever on the Pi, so do that however you see best fit.  rc.local, init script, systemd script, etc.  I have included a sample init script "blinkgpio" to put into /etc/init.d/ and enable via update-rc.d blinkgpio defaults and will add a systemd script in the future.

## LED Creation
There is an excellent introduction on [The Pi Hut](https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins) that a really good tutorial on the LED side of it, but basically just connect an LED inline with at least a 330 ohm resistor (300-1000 ohm work fine, the higher the value the dimmer the LED) and plug it into the appropriate GPIO.

## Fan Control
If you have any fans on your Pi that don't necessarily need to run 24/7, some fan control could be useful.  Mine is in a garage which is of course cold in the winter and hot in the summer, and there's not much need for the fans running all winter and I don't want to forget to plug them back in for the summer.

The script has some hysteresis in that the fan turns off if it's below the low temperature set and it only turns back on when it's above the high temp set.  If you set these to reasonable values the fan will turn off when it's too cool and as it warms back up it won't turn the fan back on until it's at the high temp.  The low and high temps will really depend on your environment but I would make the high temp fairly reasonable so it keeps the Pi cool.

You'll need a cheap relay that runs off 5v and is made for latching at 3.3v and just put it inline with the fan power cables.  Make sure you get one made for a Raspberry Pi or you'll need to put resistors inline and protect your Pi, too much current will damage GPIO pins and possibly the Pi itself.

The fan is on by DEFAULT unless the GPIO pin is high, in which case it turns off.  That means the fan must be connected on the NC (normally closed) connection on the relay.  So this way if something is wrong with the script etc, the fan will default to running.

If you need this functionality, setup your relay and set appropriate values in gatherstatus-env.sh and plug your relay into GPIO 13, which is the default in blinkgpio.py

You may also consider another minor addition of a voltage buck circuit.  If your setup is running off 12v or possibly up to 13.8v, fans run pretty fast and loud.  If you want to calm them down a bit you can put a buck circuit between the fans and the relay, or just by itself and not even use the relay if you so desire.  I've found that 8V on a 12V fan still moves a fair bit of air but makes it a lot quieter.